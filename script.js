/*
Прототипное наследование в js это возможность наследовать свойства и методы одного объекта другим. Для этого можно напрямую использовать свойство _proto_ объекта, записав туда наследуемый объект: child_proto_ = parent. 
Или можно использовать конструктор объекта: Child.prototype = parent. Тогда при создании объекта let child = new Child(), конструктор запишет наследнику в прототип объект, который наследуется.
*/

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name.toUpperCase();
    }

    set name(newName) {
        return this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        return this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        return this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }
}

let programmer1 = new Programmer("vlad", 30, 2000, ["C++", "Java", "PHP"]);
let programmer2 = new Programmer("lera", 20, 1000, ["Python, Java, Ruby"]);


console.log(programmer1);
console.log(programmer1.salary);
console.log(programmer2);
console.log(programmer2.salary);